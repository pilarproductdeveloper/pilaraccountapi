<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $table = 'bank_account';
	public $timestamps = false;
	
    protected $fillable = [
        'bank_name', 'account_number', 'holder_name', 'bank_icon'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $table = 'billing';
	public $timestamps = false;
    public $incrementing = false;   

    protected $fillable = [
        'order_id', 'user_id', 'sales_id', 'sales_devisi', 'customer_id', 'date', 'status', 'method', 'total', 'note'
    ];
}

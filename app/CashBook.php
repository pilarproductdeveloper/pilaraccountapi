<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashBook extends Model
{
    protected $table = 'cash_book';
	public $timestamps = false;
	
    protected $fillable = [
        'sales_id', 'sales_devisi', 'date', 'name',
    ];
}

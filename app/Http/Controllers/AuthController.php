<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Validator;
use Illuminate\Support\Facades\Cookie;
use Mails;
use Session;
use Auth;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
//use Lcobucci\JWT\Parser;
//use DB;
use App\Setting;

class AuthController extends Controller
{
	protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
	public function register(Request $request)
    {
		$messages = [
			'name.required' => 'Name tidak boleh kosong.',
			'email.required' => 'Email tidak boleh kosong.',
			'email.email' => 'Penulisan alamat Email tidak benar.',
			'email.unique' => 'Alamat email sudah digunakan, mohon gunakan email lainnya.',
			'password.required' => 'Password tidak boleh kosong.',
			'password_confirmation.required' => 'Konfirmasi password tidak boleh kosong.',
			'password.confirmed' => 'Konfirmasi password tidak sama.',
			'password.min' => 'Password setidaknya minimal 6 karakter.',
			'phone.required' => 'Phone tidak boleh kosong.',
			'city.required' => 'Kota tidak boleh kosong.',
		];
        $validator = Validator::make($request->all(), [
			'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
			'password_confirmation' => 'required',
			'phone' => 'required',
			'city' => 'required'
        ], $messages);
		
		if($validator->fails()){
            return response(['success' => false, 'message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
		}
		$activation_key = Str::random(50);
        $register = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
			'role' => 12,
			'phone' => $request->phone,
			'city' => $request->city,
			'activation_key' => $activation_key,
			'akademi' => $request->akademi,
			//'referral_id' => $referralid
        ]);
		//Mail::send('email.verify', ['activation_key' => $activation_key], function($message) {
          //  $message->to(Input::get('email'))
            //    ->subject('Verifikasi Email Anda');
        //});

		if($register){
			$setting=Setting::first();
			Mail::send('email.register-member', ['setting' => $setting, 'register' => $register], function($message) use ($setting){
					$message->to($setting->email_general)
						->subject('Member Baru');
				});

			Mail::send('email.register-verify', ['activation_key' => $activation_key, 'register' => $register], function($message) use ($register){
				$message->to($register->email)
					->subject('Verifikasi Email Anda');
			});
        	return response()->json([
				'success' => true,
				'message' => 'Silahkan cek email anda, kami mengirimkan link verifikasi untuk aktivasi akun anda.',
			], 201);
		} else {
			return response()->json([
				'success' => false,
				'message' => 'Pendaftaran gagal, silahkan lengkapi formulir pendaftaran.',
			], 400);
		}

    }
	public function login(Request $request)
    {
		$messages = [
			'email.required' => 'Email tidak boleh kosong.',
			'email.email' => 'Penulisan alamt Email tidak benar.',
			'password.required' => 'Password tidak boleh kosong.',
		];
		$validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required',
        ], $messages);
		
		if($validator->fails()){
            return response()->json([
				'success' => false,
				'errors' => $validator->errors()
			], 422);
        }
		$emil = $request->email;
		$password = $request->password;
		$user = User::where('email', $emil)->first();
		if($user){
			if ($user->active != NULL) {
				if(Hash::check($password, $user->password)){
					$accessToken =  $user->createToken('PilarKreatifAccount');
					
					if ($request->remember_me == true) {
						$accessToken->token->expires_at = Carbon::now()->addWeeks(1);
						$accessToken->token->save();
					}
					return response()->json([
						'success' => true,
						'errors' => false,
						'user' => $user,
						'token' => $accessToken->accessToken
					], 201);
				} else {
					return response()->json([
						'success' => false,
						'errors' => 'Maaf, email dan Password anda tidak sama.',
						'user' => '',
						'token' => ''
					], 422);
				}
			} else {
				return response()->json([
					'success' => false,
					'errors' => 'Maaf, akun anda belum aktif, silahkan cek email anda.',
					'user' => '',
					'token' => ''
				], 422);
			}
		} else {
			return response()->json([
					'success' => false,
					'errors' => 'Maaf, akun anda tidak ditemukan',
					'user' => '',
					'token' => ''
				], 422);
		}
	}
	public function logout(Request $request){
		
		//$token = $request->bearerToken();
		//$id = (new Parser())->parse($token)->getHeader('jti');
       	
		//DB::table('oauth_access_tokens')->where('id', '=', $id)->delete();
		
		//Auth::logout();
		$user = Auth::user()->token()->delete();
        return response()->json(['data' => $user]);
   }
   public function user(){
	   return response()->json(['data' => \Auth::user()]);
   }

    public function alluser(){
    	$user=User::all();
	   return response()->json(['data' => $user]);
    }

   public function profile(Request $request){
		$user_id = $request->user_id;
		$user = User::find($user_id);
		$messages = [
			'email.required' => 'Email tidak boleh kosong.',
			'email.email' => 'Penulisan alamat Email tidak benar.',
			'email.unique' => 'Alamat email sudah digunakan.',
		];
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
        ], $messages);
		if($validator->fails()){
            return response(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
        }
		$user->name = $request->name;
		$user->email = $request->email;
		$user->phone = $request->phone;
		$user->city = $request->city;

		$current_password = $user->password;   
		if(!empty($request->password)){
			$validator = Validator::make($request->all(), [
				'password_confirmation' => 'required|same:password',     
			], $messages);
			if($validator->fails()){
				return response(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
			}
		 if (Hash::check($request->password, $current_password) == false) {
			$user->password = Hash::make($request->password);;     
		  } else {
			  return response()->json(['error' => 'Password yang anda masukan sama dengan password sebelumnya.']);
		  }
		 }
		 $user->update();		
		return response()->json([
			'success' => true,
			'errors' => false,
			'message' => 'Data profile anda sudah berhasil diperbarui',
		], 201);
   }
   public function getActive(Request $request)
    {
        if(!$request->activation_key)
        {
			return response()->json(['status' => false, 'message' => 'Kode verifikasi anda salah.']);
        }

        $user = User::where('activation_key', $request->activation_key)->first();

        if (!$user)
        {
            return response()->json(['status' => false, 'message' => 'Kode verifikasi anda salah.']);
        }

        $user->active = 1;
        $user->activation_key = null;
        $user->save();

        return response()->json(['status' => true, 'message'=>'Verifikasi Berhasil.']);
	}
	public function postResend(Request $request)
	{
		$activation_key = Str::random(50);
		$data_reg = array (
			'activation_key' => $activation_key
		);
		$user = User::where('email', $request->email)->first();
		if (!$user) {
			return response()->json(['status' => false, 'message' => 'Email anda belum terdaftar.']);
		} 
		if ($user->active == 1) {
			return response()->json(['status' => true, 'message' => 'Akun anda sudah aktif dan terverifkasi.']);
		}
		$register = User::find($user->id);
		$register->update($data_reg);
		
		Mail::send('email.verify', ['activation_key' => $activation_key, 'register' => $register], function($message) use ($register){
            $message->to($register->email)
                ->subject('Verifikasi email Anda');
        });
		return response()->json(['success' => true, 'message' => 'Silahkan cek email anda, kami mengirimkan link verifikasi baru.']);
	}
}

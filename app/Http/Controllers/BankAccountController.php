<?php 
namespace App\Http\Controllers;
use App\BankAccount;

class BankAccountController extends Controller
{
    function index(){
        $package = BankAccount::all();
        if(!$package->isEmpty()){
            foreach ($package as $key => $value) {
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
}
<?php 
namespace App\Http\Controllers;
use App\Billing;
use App\Order;
use App\BankAccount;
use App\TransactionCategory;
use App\CashBook;
use Illuminate\Http\Request;

class billingController extends Controller
{
    public function index(){
        $package = Billing::all();
       
        if(!$package->isEmpty()){
            foreach ($package as $key => $value) {
                unset($value['sales_id']);
                unset($value['sales_divisi']);
                unset($value['transaction_category']);
                unset($value['cash_book']);
                unset($value['customer_id']);
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
    
    public function store(Request $request)
	{	
			$save = new Billing();
			$save->id = $request->id;
			$save->order_id = $request->order_id;
			$save->customer_id = NULL;
			$save->user_id = $request->user_id;
			$save->total = str_replace('.', '', strtolower($request->total));
			$save->accname = $request->accname;
			$save->created_at = $request->created_at;
			$save->date = $request->date;
			$transaction_category = TransactionCategory::where('slug', $request->transaction_category)->first();
			$save->transaction_category = $transaction_category->id;
			$bank = BankAccount::find($request->bank_account);
			$save->method = $bank->bank_name;
			$save->bank_account = $request->bank_account;
			$cash_book = CashBook::where('slug', $request->cash_book)->first();
			$save->cash_book = $cash_book->id;
			$save->note = $request->note;
			$save->save();
			
			$order = Order::where('invoice', $request->invoice)->whereMonth('date', date('m', $request->time))->first();


			$billings = Billing::where('order_id', $order->id)->pluck('total')->toArray();
            $billingtotal = array_sum($billings);
            
			if(($order->total+$order->unique_code) == $billingtotal) {
				$billing = Billing::find($save->id);
				if($order->user_id != NULL){	
					$billing->status = 'PROCESSED';
					$order->status = 'PROCESSED';
				} else {
					$billing->status = 'PAID';
					$order->status = 'PAID';
				}
				$billing->update();
				$order->update();
			} else {
				$billing = Billing::find($save->id);
				$billing->status = 'DOWNPAYMENT';
				$billing->update();
				$order->status = 'DOWNPAYMENT';
				$order->update();
			}

		return response()->json(['data' => $billing]);
	}
}
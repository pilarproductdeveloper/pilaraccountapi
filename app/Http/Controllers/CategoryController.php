<?php 
namespace App\Http\Controllers;
use App\Category;

class CategoryController extends Controller
{
    function index(){
       $category = Category::pluck('name','id');
        if(!$category->isEmpty()){
            foreach ($category as $key => $value) {
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
}
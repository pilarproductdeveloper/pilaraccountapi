<?php 
namespace App\Http\Controllers;
use App\License;
use App\Site;
use Illuminate\Http\Request;

class LicenseController extends Controller
{
    public function index(){
        $license = License::all();
        if(!$license->isEmpty()){
            foreach ($license as $key => $value) {
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
    public function postLicense(Request $request){
        $license = new License();
        $license->id = $request->id;
        $license->user_id = $request->user_id;
        $license->key = $request->key;
        $license->date = $request->date;
        $license->varian_id = $request->varian_id;
        $license->site = $request->site;
        $license->expired_date = $request->expired_date;
        $license->status = $request->status;
        $license->web_type = $request->web_type;
        $license->save();

        return response()->json(['data' => $license]);
    }
    public function site(){
        $site = Site::all();
        if(!$site->isEmpty()){
            foreach ($site as $key => $value) {
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
    public function sitedelete(Request $request){
        $item = Site::find($request->site_id);
        $item->delete();
        return response()->json(['status' => 'true']);
    }
}
<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Coupon;
use App\Billing;
use Illuminate\Support\Facades\Hash;



class OrderController extends Controller
{
    public function index(){
        $package = Order::all();
        if(!$package->isEmpty()){
            foreach ($package as $key => $value) {
                unset($value['sales_id']);
                unset($value['sales_divisi']);
                unset($value['customer_id']);
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }

    public function getorder($id){
        $package = Order::where('user_id',$id)->get();
        if(!$package->isEmpty()){
            foreach ($package as $key => $value) {
                unset($value['sales_id']);
                unset($value['sales_divisi']);
                unset($value['customer_id']);
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
   public function post(Request $request){
        $order = new Order();
        $order->id = $request->id;
        $order->user_id = $request->user_id;
        $order->voucher_id = $request->voucher_id;
        $order->date = $request->date;
        $order->created_at = $request->created_at;
        $order->invoice_perfix = $request->invoice_perfix;
        $order->due_date = $request->due_date; 
        $order->invoice = $request->invoice;
        $order->package_detail = $request->package_detail;
        $order->discount = $request->discount;
        $order->unique_code = $request->unique_code;
        $order->type = $request->type;
        $order->total = $request->total;
        $order->status = $request->status;
        $order->pick_service = $request->pick_service;
        $order->akademi = $request->akademi;
        if ( $request->tracker_id=="") {
            $order->tracker_id = NULL;
        }else{
            $order->tracker_id = $request->tracker_id;
        }
        $order->save();
        return response()->json(['data' => $order]);
    }
    public function coupon(Request $request)
    {
        $code = $request->code;
		if($code != 'empty'){
			$voucher = Coupon::where('code', $code)->first();
			if(!$voucher){
				return response()->json(
					[
					'success' => false, 
					'message' => 'Kode Coupon tidak Tersedia.',
					]
				);
			}else if($voucher->expired_date == date('Y-m-d')){
				return response()->json(
					[
					'success' => false, 
					'message' => 'Kode Coupon expired.',
					]
				);
			} else {
				return response()->json(
					[
					'success' => true, 
					'message' => 'Berhasil.',
                    'id' => $voucher->id,
                    'code' => $voucher->code,
                    'price' => $voucher->price,
                    'type' => $voucher->type,
                    'expired_date' => $voucher->expired_date,
                    'product' => json_decode($voucher->product),
					]
				);
			}
		} else {
			return response()->json(
				[
				'success' => false, 
				'message' => 'Form Coupon Kosong.',
				]
			);
		}
	}


    public function orderdelete(Request $request){
        $order = Order::find($id);
        $order->delete();
        $billing = Billing::where('order_id', $id)->get();
        foreach ($billing as $value){
            $billing_update = Billing::find($value->id);
            $billing_update->order_id = NULL;
            $billing_update->update();
        }
        return response()->json(['status' => 'true']);
    }

    public function user($id){
        $user = User::find($id);

        return response()->json(['data' => $user]);

    }
}
<?php 
namespace App\Http\Controllers;
use App\Package;
use App\Variation;
use App\JasaWebsite;

class PackageController extends Controller
{
   public function index(){
        $package = Package::all();
        if(!$package->isEmpty()){
            foreach ($package as $key => $value) {
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
    public function varian(){
        $variation = Variation::all();
        if(!$variation->isEmpty()){
            foreach ($variation as $key => $value) {
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
    public function JasaWebsite(){
        $jasawebsite = JasaWebsite::all();
        if(!$jasawebsite->isEmpty()){
            foreach ($jasawebsite as $key => $value) {
                $result[$key] = $value;
            }
        } else { $result = []; }
        return response()->json(['data' => $result]);
    }
}
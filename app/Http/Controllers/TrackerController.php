<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Tracker;

class TrackerController extends Controller
{
   public function post(Request $request){
        $tracker = new Tracker();
        $tracker->utm_source=$request->utm_source;
        $tracker->utm_campign=$request->utm_campign;
        $tracker->utm_content=$request->utm_content;
        $tracker->utm_medium=$request->utm_medium;
        $tracker->utm_term=$request->utm_term;
        $tracker->save();


        return response()->json(['data' => $tracker]);
    }

}
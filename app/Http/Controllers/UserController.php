<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function storeUser(Request $request){
		$user = new User();
		if (User::where('email', '=', $request->email)->exists()) {
			return response()->json(['errors' => 'Email yang anda masukan sudah terdaftar, silahkan menggunakan email yang lain.']);
		}
		
		$user->name = $request->name;
		$user->email = $request->email;
		$user->company = $request->company;
		$user->phone = $request->phone;
		if(!empty($request->password)){
			$user->password = Hash::make($request->password);
		}

		if(!empty($request->tracker_id)){
			$user->tracker_id = $request->tracker_id;
		}

		if(!empty($request->akademi)){
			$user->akademi = $request->akademi;
		}
		$user->role = $request->role;
		$user->city = $request->city;
		$user->save();
        return response()->json(['data' => $user]);
   }
   public function updateUser(Request $request){
	$errors = '';
	$user = User::find($request->user_id);

	if($user->email != $request->email){
		if (User::where('email', '=', $request->email)->exists()) {
			return response()->json(['errors' => 'Email yang anda masukan sudah terdaftar, silahkan menggunakan email yang lain.']);
		}
	}
	$user->name = $request->name;
	$user->email = $request->email;
	$user->company = $request->company;
	$user->phone = $request->phone;
	if(!empty($request->password)){
		$user->password = Hash::make($request->password);
	}
	$user->city = $request->city;
	$user->update();
	return response()->json(['data' => $user]);
}
   public function checkPassword(Request $request){
	if (!Hash::check($request->get('old_password'), Auth::user()->password)) {
		return response()->json(['error' => 'Old Password yang anda masukan tidak sesuai.']);
	}
   }
   public function verifyAdmin(Request $request){
		$user = User::find($request->user_id);
		if ($request->approve == 'true') {
			$user->role=2;
			$user->active = 1;
			$user->activation_key = null;
		}else{
			$user->active = 2;
		}
		$user->save();
		return response()->json(['data' => $user]);
   }
}

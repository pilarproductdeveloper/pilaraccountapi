<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Requests;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PublicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $secret_key = explode(' ',$request->header('Authorization'));
        
        if (env('API_APP_PUBLIC') !=  $secret_key[1]) {
        // if (env('API_APP_PUBLIC') !=  'DKZo9I39UXq3qPVutRVZW2CH3YtTgeGxl9pFTKCFC2RWO22jOO') {

             return response()->json('Unauthorized', 401);
        } 
        

        return $next($request);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JasaWebsite extends Model
{
    protected $table = 'jasa_website';
	public $timestamps = false;	
	
    protected $fillable = [
        'name', 'slug'
    ];
}

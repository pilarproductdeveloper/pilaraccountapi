<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{

    protected $table = 'license';
	public $timestamps = false;
    public $incrementing = false;
    
    protected $fillable = [
        'key', 'token', 'user_id', 'varian_id', 'date', 'status', 'expired_date', 'site'
    ];
}

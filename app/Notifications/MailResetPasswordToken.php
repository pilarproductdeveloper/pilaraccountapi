<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordToken extends Notification
{
    use Queueable;
	public $token;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $name;

    public function __construct($token, $name)
    {
        $this->token = $token;
        $this->name = $name;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hai '.$this->name.',')
                    ->subject("Reset your password")
                    ->line("Kami menerima permintaan pengaturan ulang kata sandi akun anda. Silahkan klik tombol dibawah untuk melanjurkan.")
                    ->action('Reset Password', 'https://app.pilarakademi.com/password/reset/'. $this->token)
                    ->line('Jika Anda tidak meminta pengaturan ulang kata sandi, abaikan email ini atau hubungi kami jika ada pertanyaan.');
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

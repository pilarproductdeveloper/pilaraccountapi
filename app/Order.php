<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
	public $timestamps = false;
    public $incrementing = false;   

    protected $fillable = [
        'sales_id', 'sales_divisi', 'user_id', 'customer_id', 'voucher_id', 'date', 'package_detail', 'total', 'dicount', 'status', 'type', 'note','akademi'
    ];
}

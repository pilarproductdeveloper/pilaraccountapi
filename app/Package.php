<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
   	protected $table = 'package';
	public $timestamps = false; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'download_id', 'cat_id', 'name', 'desc', 'image', 'site_price', 'slug', 'type'
    ];
}

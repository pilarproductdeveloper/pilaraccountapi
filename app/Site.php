<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'site';
	public $timestamps = false;
	
    protected $fillable = [
        'user_id', 'license_id', 'date', 'domain'
    ];
	function get_license(){
        return $this->belongsTo('App\License','license_id','id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracker extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'tracker';
	public $timestamps = false;
	


}

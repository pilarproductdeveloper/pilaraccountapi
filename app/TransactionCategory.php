<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionCategory extends Model
{
    protected $table = 'transaction_category';
	public $timestamps = false;
	
    protected $fillable = [
        'name', 'type'
    ];
}

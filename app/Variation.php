<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
	protected $table = 'variations';
    public $timestamps = false;
    
    protected $fillable = [
        'title', 'site', 'price', 'renewal', 'commission', 'lifetime_commission'
    ];

}

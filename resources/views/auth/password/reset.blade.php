@extends('template.content-login')
@section('content')
<body class="hold-transition login-page">
<div class="login-box skin-red">
  <div class="login-logo">
    <a href="{{ URL('/') }}"><img src="{!! URL('media/logo-black/logo-black.png') !!}" /></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
  
  	@if($message=Session::get('not_user'))
        <div class="callout callout-danger callout-flat">
            {{$message}}
            <a href="{{ route('register') }}">Daftar sekarang.</a>
        </div>
    @elseif($message=Session::get('invalid'))
        <div class="callout callout-success callout-flat">
            {{$message}}
        </div>
    @endif
    <p class="login-box-msg">Masukan email untuk mengatur ulang password anda.</p>
    <form method="POST" action="{{ route('password.request') }}">
      {{ csrf_field() }}

      <input type="hidden" name="token" value="{{ $token }}">

      <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
        <input type="email" class="form-control" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <span class="help-block">
                {{ $errors->first('email') }}
            </span>
        @endif
      </div>
      <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         @if ($errors->has('password'))
            <span class="help-block">
                {{ $errors->first('password') }}
            </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation" required>
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
         @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-danger btn-full-width btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
	</form>
    </div>
</div>
@endsection



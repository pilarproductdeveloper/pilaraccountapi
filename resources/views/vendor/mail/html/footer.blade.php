<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td class="content-top" align="center">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
            <tr>
                <td align="center"><p>PT. Pilar Kreatif Teknologi<br>Denpasar - Bali <br> Indonesia<p></td>
            </tr>
        </table>
    </td>
</tr>

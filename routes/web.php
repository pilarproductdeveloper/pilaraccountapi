<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
	$router->post('/register','AuthController@register');
	$router->post('/login','AuthController@login');
	$router->post('/logout','AuthController@logout');
	$router->post('/password/email', 'PasswordController@postEmail');
	$router->post('/password/reset', 'PasswordController@reset');
	$router->post('/password/reset/post', 'PasswordController@postReset');
	$router->post('/verify/email', 'AuthController@getActive');
	$router->post('/verify/resendcode', 'AuthController@postResend');
	$router->post('frontend/login/cart','AuthController@login');
	$router->post('frontend/post/user', 'UserController@storeuser');
	

	$router->group(['middleware' => 'public'], function() use ($router){
		$router->get('frontend/get/package', 'PackageController@index');
		$router->get('frontend/get/variation', 'PackageController@varian');
		$router->get('frontend/get/bank', 'BankAccountController@index');
		$router->get('frontend/get/order', 'OrderController@index');
		$router->get('frontend/get/orderkey', 'OrderController@index');
		$router->post('frontend/post/order', 'OrderController@post');
		$router->get('frontend/order/user/{id}', 'OrderController@user');
		$router->post('frontend/post/license', 'LicenseController@postLicense');
		$router->get('frontend/get/jasawebsite', 'PackageController@JasaWebsite');
		$router->get('frontend/get/billing', 'BillingController@index');
		$router->post('frontend/post/billing', 'BillingController@store');
		$router->get('frontend/get/alluser', 'AuthController@alluser');
		$router->post('frontend/get/order/coupon', 'OrderController@coupon');
		$router->get('frontend/get/coupon', 'CouponController@coupon');
		$router->post('frontend/post/tracker', 'TrackerController@post');

	});
	$router->group(['middleware' => 'auth'], function() use ($router){
		$router->get('get/user', 'AuthController@user');
		$router->get('get/category', 'CategoryController@index');
		$router->get('get/alluser', 'AuthController@alluser');
		$router->post('post/user', 'UserController@storeUser');
		$router->post('update/user', 'UserController@updateUser');
		$router->post('verify/admin', 'UserController@verifyAdmin');
		$router->post('check/password', 'UserController@checkPassword');
		$router->post('profile/edit', 'AuthController@profile');
	    $router->get('get/package', 'PackageController@index');
		$router->get('get/variation', 'PackageController@varian');
		$router->get('get/jasawebsite', 'PackageController@JasaWebsite');
		$router->get('get/order', 'OrderController@index');
		$router->post('delete/order', 'OrderController@orderdelate');
		$router->post('get/order/coupon', 'OrderController@coupon');
		$router->post('post/order', 'OrderController@post');
		$router->get('get/billing', 'BillingController@index');
		$router->post('post/billing', 'BillingController@store');
		$router->get('get/bank', 'BankAccountController@index');
		$router->get('get/license', 'LicenseController@index');
		$router->post('post/license', 'LicenseController@postLicense');
		$router->get('get/site', 'LicenseController@site');
		$router->post('delete/site', 'LicenseController@sitedelete');
		$router->get('get/order/{id}', 'OrderController@getorder');



	});
});
